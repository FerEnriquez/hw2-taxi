defmodule TaxiBeWeb.TaxiAllocationJob do
  use GenServer

  def start_link(request, name) do
      GenServer.start_link(__MODULE__, request, name: name)
  end
  def init(request) do
    #Lo voy a esperar 5000seg
    #Process.send_after(self(), :compute_ride_fare, 5000)
    Process.send(self(), :initial_step, [:nosuspend])
    {:ok, %{request: request}}
  end
  def handle_info(:initial_step, %{request: request}) do
    bComputeRideFare = Task.async(fn -> compute_ride_fare(request) end)
    candidates = select_candidate_taxis(request)
    Task.await(bComputeRideFare)
    {:noreply, %{request: request, candidates: candidates}}
  end
  def compute_ride_fare(request) do
      %{
          "pickup_address" => pickup_address,
          "dropoff_address" => dropoff_address
      } = request

      coord1 = TaxiBeWeb.Geolocation.geocode(pickup_address)
      coord2 = TaxiBeWeb.Geolocation.geocode(dropoff_address)
      {distance, _duration} = TaxiBeWeb.Geolocation.distance_and_duration(coord1, coord2)

      TaxiBeWeb.Endpoint.broadcast("customer:frodo", "booking_request", %{msg: "Ride fare: #{Float.ceil(distance/300)}"})
      TaxiBeWeb.Endpoint.broadcast("customer:frodo", "booking_request", %{msg: "arcangeles is your Taxi"})
      TaxiBeWeb.Endpoint.broadcast("driver:angelopolis", "booking_request", %{msg: "Can you give me a lift?"})
  end
  def select_candidate_taxis(%{"pickup_address" => _pickup_address}) do
      [
          %{nickname: "angelopolis", latitude: 19.0319783, longitude: -98.2349368},
          %{nickname: "arcangeles", latitude: 19.0061167, longitude: -98.2697737},
          %{nickname: "destino", latitude: 19.0092933, longitude: -98.2473716}
      ]
  end

end