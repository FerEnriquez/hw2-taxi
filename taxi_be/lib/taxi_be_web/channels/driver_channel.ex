defmodule TaxiBeWeb.DriverChannel do
  use TaxiBeWeb, :channel

  def join("driver:"<> _driver, payload, socket) do
    {:ok, socket}
  end
end
