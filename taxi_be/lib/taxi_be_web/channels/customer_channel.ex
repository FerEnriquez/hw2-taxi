defmodule TaxiBeWeb.CustomerChannel do
  use TaxiBeWeb, :channel

  def join("customer:"<> _customer, payload, socket) do
      {:ok, socket}
  end
end
