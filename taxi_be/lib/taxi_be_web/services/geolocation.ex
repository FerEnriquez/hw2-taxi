defmodule TaxiBeWeb.Geolocation do
    @geocodingURL "https://api.mapbox.com/geocoding/v5/mapbox.places/"
    @directionsURL "https://api.mapbox.com/directions/v5/mapbox/driving/"
    @directionsMatrixURL "https://api.mapbox.com/directions-matrix/v1/mapbox/driving/"

    @token "pk.eyJ1IjoibGdiYW51ZWxvcyIsImEiOiJjanVlZm02ZmEwMWJ0M3lwZG43aGh5dHkwIn0.bkjOWEzJQMkfuoCv82x-Og"

    def geocode(address) do
        %{body: body} = HTTPoison.get!(
            @geocodingURL <>
            URI.encode(address) <>
            ".json?limit=1&access_token=" <>
            @token )
        
        coord = body
        |> Jason.decode!
        |> Map.fetch!("features")
        |> Enum.at(0)
        |> Map.fetch!("center")
        
        coord
    end

    def distance_and_duration(origin_coord, destination_coord) do
        %{body: body} = HTTPoison.get!(@directionsURL <> "#{Enum.join(origin_coord, ",")};#{Enum.join(destination_coord, ",")}"<> "?access_token=" <> @token)
    
        %{"duration" => duration, "distance" => distance} =
            body
            |> Jason.decode!
            |> Map.fetch!("routes")
            |> Enum.at(0)
    
        {distance, duration}
    end

    def closest(clientPosition) do
        [
            %{nickname: "angelopolis", latitude: 19.0319783, longitude: -98.2349368},
            %{nickname: "arcangeles", latitude: 19.0061167, longitude: -98.2697737},
            %{nickname: "destino", latitude: 19.0092933, longitude: -98.2473716}
        ]

        %{body: body} = HTTPoison.get!(
            @directionsMatrixURL 
            <> "#{Enum.join(clientPosition, ",")} )" 
            <> ";-98.2349368,19.0319783;-98.2697737,19.0061167;-98.2473716,19.0092933"    
            <> "?source=1&annotations=distance,duration&access_token=" 
            <> @token)

        distance = body
            |> Jason.decode!
            |> Map.fetch!("distances")
            |> Enum.min(0)

        distance
    end
end