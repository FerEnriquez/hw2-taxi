# defmodule TaxiBeWeb.BookingController do
#     use TaxiBeWeb, :controller

#     def create(conn, %{ "pickup_address" => pickup_address, "dropoff_address" => dropoff_address}) do
#         coord1 = TaxiBeWeb.Geolocation.geocode(pickup_address)
#         |> IO.inspect
#         coord2 = TaxiBeWeb.Geolocation.geocode(dropoff_address)
#         |> IO.inspect

#         {distance,_} = TaxiBeWeb.Geolocation.distance_and_duration(coord1, coord2)
#         IO.inspect "Ride fare: #{Float.ceil(distance/300)}"

#         TaxiBeWeb.Endpoint.broadcast("customer:frodo", "booking_request", %{msg: "Ride fare: #{Float.ceil(distance/300)}"})
#         json(conn, %{msg: "We are working hard to serve your request"})
#     end
# end
defmodule TaxiBeWeb.BookingController do
    use TaxiBeWeb, :controller

    alias TaxiBeWeb.TaxiAllocationJob

    def create(conn, request) do
    name = UUID.uuid1()
                                                    #Toma el mapa y agrega una nueva key "booking_id"
    TaxiBeWeb.TaxiAllocationJob.start_link(request |> Map.put("booking_id", name), String.to_atom(name))

    json(
        conn
        |> merge_resp_headers([{"location", Routes.booking_path(conn, :show, name)}])
        |> put_status(:created),
        %{msg: "We are processing your request"}
       
    )
end
end