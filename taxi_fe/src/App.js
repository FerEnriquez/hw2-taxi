import React from 'react';
import Reflux from 'reflux';
import axios from 'axios';
import socket from './services/taxi_socket';

let Actions = Reflux.createActions(["appendMessage", {
    submitBookingRequest: {children: ['completed', 'failed']}
}]);

Actions.submitBookingRequest.listen(function(pickup_address, dropoff_address) {
  var that = this;
  axios.post('http://localhost:4000/api/bookings', {
        pickup_address: pickup_address,
        dropoff_address: dropoff_address
    })
    .then(res => that.completed(res.data))
    .catch(this.failed);
});

class AppStore extends Reflux.Store {
  constructor() {
    super();
    this.state = {
      pickup_address: "Via Atlixcayotl 5718, Puebla, Mexico",
      dropoff_address: "CAPU, Puebla, Mexico",
      messages: ""
    };
    this.listenables = Actions;
  }
  onAppendMessage(msg) {
    this.setState({messages: ''.concat(this.state.messages, msg, '\n')});
  }
  onSubmitBookingRequestCompleted(response) {
    this.setState({messages: ''.concat(this.state.messages, response.msg, '\n')});
  }
  onSubmitBookingRequestFailed() {
    console.error("oops");
  }
}

class App extends Reflux.Component {
  constructor(props) {
    super(props);
    this.store = AppStore;
  }
  componentDidMount() {
    let channel = socket.channel("customer:" + this.props.username, {token: "123"});
    channel.on("booking_request", data => Actions.appendMessage(data.msg));
    channel.join()
      .receive("ok", ({messages}) => console.log("catching up", messages) )
      .receive("error", ({reason}) => console.log("failed join", reason) )
      .receive("timeout", () => console.log("Networking issue. Still waiting..."));
  }
  
  render() {
    return (
      <div>
          <div>
            <label>Pickup address </label>
            <input onChange={ ev => this.setState({pickup_address: ev.target.value}) } value={this.state.pickup_address}></input>
          </div>
          <div>
            <label>Drop off address </label>
            <input onChange={ ev => this.setState({dropoff_address: ev.target.value}) } value={this.state.dropoff_address}></input>
          </div>
          <button onClick={ () => Actions.submitBookingRequest(this.state.pickup_address, this.state.dropoff_address) }>Submit booking request</button>
          <div>
            <textarea 
              value={ this.state.messages } 
              style={{minWidth: "400px", minHeight: "200px"}}
              readOnly
            />
          </div>
      </div>
    );
  }
}

export default App;