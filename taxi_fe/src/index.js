import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Driver from './components/Driver'
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <div>
        <App username="frodo" />
        <Driver username="Angelopolis"/>
        <Driver username="Via San Angel"/>
    </div>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
