import React, {Component} from 'react';
import socket from '../services/taxi_socket';

export default class Driver extends Component {
    constructor(props) {
        super(props)
        this.state = {message: "", visible: false}
    }

    // Este se ejecuta cuando ya te se completo
    componentDidMount() {
        const that = this;
        let channel = socket.channel("driver:" + this.props.username, {token: "123"});
        channel.on("booking_request", function(data) {
            that.setState({message: data.msg, visible: true});
        });
        channel.join()
            .receive("ok", ({messages}) => console.log("catching up", messages) )
            .receive("error", ({reason}) => console.log("failed join", reason) )
            .receive("timeout", () => console.log("Networking issue. Still waiting..."));
    }

    render() {
        return (
            <div style={{backgroundColor: 'Lavender', height: '200px'}}>
                <h3>{this.props.username}</h3>
                {
                    this.state.visible ?
                        <div>
                            {this.state.message} <p/>
                            <button>Accept</button>
                            <button>Reject</button>
                        </div>
                    :
                        <div> </div>
                }                
            </div>
        );
    }
}